package m.co.rh.id.a_news_provider.app.constants;

public class Routes {
    public static final String HOME_PAGE = "/";
    public static final String SETTINGS_PAGE = "/settings";

    private Routes(){}
}
